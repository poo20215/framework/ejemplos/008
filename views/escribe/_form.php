<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Autores;
use app\models\Libros;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Escribe */
/* @var $form yii\widgets\ActiveForm */

$autores= Autores::find()->all();
$listadoAutores= ArrayHelper::map($autores, 'id', 'nombre');
$libros= Libros::find()->all();
$listadoLibros= ArrayHelper::map($libros, 'id', 'nombre');
?>

<div class="escribe-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'autor')->dropDownList($listadoAutores) ?>

    <?= $form->field($model, 'libro')->dropDownList($listadoLibros) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
