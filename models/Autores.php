<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autores".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $fechaNacimiento
 *
 * @property Escribe[] $escribes
 * @property Leer[] $leers
 * @property Libros[] $libros
 * @property Libros[] $libros0
 */
class Autores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaNacimiento'], 'safe'],
            [['nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'fechaNacimiento' => 'Fecha Nacimiento',
        ];
    }

    /**
     * Gets query for [[Escribes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEscribes()
    {
        return $this->hasMany(Escribe::className(), ['autor' => 'id']);
    }

    /**
     * Gets query for [[Leers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLeers()
    {
        return $this->hasMany(Leer::className(), ['autor' => 'id']);
    }

    /**
     * Gets query for [[Libros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLibros()
    {
        return $this->hasMany(Libros::className(), ['id' => 'libro'])->viaTable('escribe', ['autor' => 'id']);
    }

    /**
     * Gets query for [[Libros0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLibros0()
    {
        return $this->hasMany(Libros::className(), ['id' => 'libro'])->viaTable('leer', ['autor' => 'id']);
    }
}
